import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)

export default new Router({
  mode:"history",
  routes: [
    {
      path: '/',
      name: 'name1',
      component:()=>import("@/components/A1"),
    },
    {
      path: '/2',
      name: 'name2',
      component:()=>import("@/components/A2"),
    },
    {
      path: '/3',
      name: 'name3',
      component:()=>import("@/components/A3"),
    },
    {
      path: '/4',
      name: 'name4',
      component:()=>import("@/components/A4"),
    },
    {
      path: '/5',
      name: 'name5',
      component:()=>import("@/components/A5"),
    },
  ]
})

